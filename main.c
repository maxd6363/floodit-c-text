#include "game.h"

#include <stdio.h>
#include "setting.h"

int main(int argc, char const *argv[]){
	Settings settings = {.maxRound=23,.gridSize=12,.numberColors=6};
	if(argc > 1){
		settings = setting_load(argv[1]);
	}
	//game_entry();
	game_start(settings);
	return 0;
}