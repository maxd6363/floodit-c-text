# FloodIt - C Text

Flood It game in c, playable in the terminal

## Usage : 

    ./make
    ./out settings.conf

OR

    chmod 755 run.sh
    ./run.sh


## settings.conf :
* Number of rounds to win
* Size of the grid
* Number of colors, maximum 7 (limited by ANSI colors)
