#include "error.h"

#include "color.h"
#include <stdio.h>


void error(char * file, int line, char * function){
	fprintf(stderr,RED "%s : %d \t %s\n" RESET,file, line, function);
}